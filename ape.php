<?php
class Ape extends Animal {
    public $legs = 2;
    public function yell() {
        return "Auooo"; 
    }
}

$ape = new Ape("kera sakti");
echo "Name : $ape->name <br>";
echo "Legs : $ape->legs <br>";
echo "Cold Blooded : $ape->cold_blooded <br>";
echo "Yell : " . $ape->yell() . "<br>";

?>
