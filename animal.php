<?php
class Animal
{
  public $legs = 4;
  public $cold_blooded = "no";
  public $name;
  public function __construct($name) {
    $this->name = $name;
  }
} 
$sheep = new Animal("shaun");

echo "Name : $sheep->name <br>"; // "shaun"
echo "Legs : $sheep->legs <br>"; // 4
echo "Cold Blooded : $sheep->cold_blooded <br> <br>"; // "no"

?>