<?php
class Frog extends Animal {
    public $legs = 4;
    public function jump() {
        return "hop hop"; 
    }
}

$frog = new Frog("buduk");
echo "Name : $frog->name <br>";
echo "Legs : $frog->legs <br>";
echo "Cold Blooded : $frog->cold_blooded <br>";
echo "Jump : " .  $frog->jump() . "<br> <br>";

?>
